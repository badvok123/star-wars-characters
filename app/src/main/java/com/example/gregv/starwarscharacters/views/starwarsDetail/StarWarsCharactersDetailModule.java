package com.example.gregv.starwarscharacters.views.starwarsDetail;

import com.example.gregv.starwarscharacters.rx.SchedulersFacade;

import dagger.Module;
import dagger.Provides;

@Module
public class StarWarsCharactersDetailModule {

    @Provides
    StarWarsCharactersDetailPresenter providePresenter(
            StarWarsCharactersDetailContract.StarWarsCharactersDetailView view,
             SchedulersFacade schedulersFacade
    ){
        return new StarWarsCharactersDetailPresenter(view, schedulersFacade);
    }

}
