package com.example.gregv.starwarscharacters.views.starwars;

import com.example.gregv.starwarscharacters.networking.StarWarsCharactersRepository;
import com.example.gregv.starwarscharacters.rx.SchedulersFacade;

import dagger.Module;
import dagger.Provides;

@Module
public class StarWarsCharactersModule {


    @Provides
    StarWarsCharactersPresenter providePresenter(
            StarWarsCharactersContract.StarWarsCharactersView view,
            StarWarsCharactersUseCase starWarsCharactersUseCase,
            SchedulersFacade schedulersFacade
    ){
        return new StarWarsCharactersPresenter(view,starWarsCharactersUseCase,schedulersFacade );
    }

}
