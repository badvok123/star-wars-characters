package com.example.gregv.starwarscharacters.model;

import com.google.gson.annotations.SerializedName;

public class StarWarsCharactersResponse {

    @SerializedName("name")
    public String name;

    @SerializedName("height")
    public String height;

    @SerializedName("mass")
    public String mass;

    @SerializedName("created")
    public String dateCreated;


    public StarWarsCharactersResponse(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
