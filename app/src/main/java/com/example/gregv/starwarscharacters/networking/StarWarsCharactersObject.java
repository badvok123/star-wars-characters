package com.example.gregv.starwarscharacters.networking;

import com.example.gregv.starwarscharacters.model.StarWarsCharactersResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StarWarsCharactersObject {

    @SerializedName("results")
    public List<StarWarsCharactersResponse> results;

}
