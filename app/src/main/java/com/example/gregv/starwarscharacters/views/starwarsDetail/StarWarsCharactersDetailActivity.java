package com.example.gregv.starwarscharacters.views.starwarsDetail;

import android.os.Bundle;
import android.widget.TextView;

import com.example.gregv.starwarscharacters.R;
import com.example.gregv.starwarscharacters.base.BaseActivity;
import com.example.gregv.starwarscharacters.util.Constants;
import com.example.gregv.starwarscharacters.util.Helpers;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

public class StarWarsCharactersDetailActivity extends BaseActivity implements
    StarWarsCharactersDetailContract.StarWarsCharactersDetailView{

    String name ,height, mass, dateCreated;

    @Inject StarWarsCharactersDetailPresenter presenter;

    @BindView(R.id.txt_name)TextView txtName;
    @BindView(R.id.txt_height)TextView txtHeight;
    @BindView(R.id.txt_mass)TextView txtMass;
    @BindView(R.id.txt_created_date)TextView txtDateCreated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        getBundle();
        setTextViews();
        setTitle(R.string.star_wars_title);

    }

    private void getBundle(){
        Bundle bundle = new Bundle();
        bundle = getIntent().getBundleExtra(Constants.StarWarsCharacterBundle.BUNDLE_KEY);
        name = bundle.getString(Constants.StarWarsCharacterBundle.NAME);
        height = bundle.getString(Constants.StarWarsCharacterBundle.HEIGHT);
        mass = bundle.getString(Constants.StarWarsCharacterBundle.MASS);
        dateCreated = bundle.getString(Constants.StarWarsCharacterBundle.CREATED_DATE);
        dateCreated = Helpers.formatDate(dateCreated);
    }


    private void setTextViews(){
        txtName.setText(name);
        txtHeight.setText(height);
        txtMass.setText(mass);
        txtDateCreated.setText(dateCreated);
    }

}
