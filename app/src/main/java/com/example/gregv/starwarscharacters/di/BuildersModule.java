package com.example.gregv.starwarscharacters.di;

import com.example.gregv.starwarscharacters.networking.ServicesModule;
import com.example.gregv.starwarscharacters.views.starwars.StarWarsCharactersActivity;
import com.example.gregv.starwarscharacters.views.starwars.StarWarsCharactersModule;
import com.example.gregv.starwarscharacters.views.starwars.StarWarsCharactersViewModule;
import com.example.gregv.starwarscharacters.views.starwarsDetail.StarWarsCharactersDetailActivity;
import com.example.gregv.starwarscharacters.views.starwarsDetail.StarWarsCharactersDetailModule;
import com.example.gregv.starwarscharacters.views.starwarsDetail.StarWarsCharactersDetailViewModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {


    @ContributesAndroidInjector(modules = {
            StarWarsCharactersViewModule.class,
            StarWarsCharactersModule.class,
            ServicesModule.class
    })


    abstract StarWarsCharactersActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {
            StarWarsCharactersDetailViewModule.class,
            StarWarsCharactersDetailModule.class,
    })

    abstract StarWarsCharactersDetailActivity bindDetailActivity();
}
