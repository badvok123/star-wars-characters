package com.example.gregv.starwarscharacters.di;

import com.example.gregv.starwarscharacters.base.AppDelegate;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        BuildersModule.class
})
public interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder appDelegate(AppDelegate appDelegate);
        AppComponent build();
    }

    void inject(AppDelegate appDelegate);

}
