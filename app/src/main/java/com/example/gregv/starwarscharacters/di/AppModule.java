package com.example.gregv.starwarscharacters.di;

import android.content.Context;

import com.example.gregv.starwarscharacters.base.AppDelegate;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides Context provideContext(AppDelegate appDelegate){
        return appDelegate.getApplicationContext();
    }

}
