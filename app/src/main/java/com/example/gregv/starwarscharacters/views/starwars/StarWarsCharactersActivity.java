package com.example.gregv.starwarscharacters.views.starwars;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.gregv.starwarscharacters.R;
import com.example.gregv.starwarscharacters.base.BaseActivity;
import com.example.gregv.starwarscharacters.model.StarWarsCharactersResponse;
import com.example.gregv.starwarscharacters.networking.StarWarsCharactersObject;
import com.example.gregv.starwarscharacters.util.Constants;
import com.example.gregv.starwarscharacters.views.starwarsDetail.StarWarsCharactersDetailActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import timber.log.Timber;

public class StarWarsCharactersActivity extends BaseActivity implements
        StarWarsCharactersContract.StarWarsCharactersView{

    @Inject StarWarsCharactersPresenter presenter;

    @BindView(R.id.loading_indicator)
    ProgressBar loadingIndicator;

    @BindView(R.id.btn_retry)
    Button btnRetry;

    @BindView(R.id.recycler_star_wars_characters)
    RecyclerView starWarsCharactersRecyclerView;

    private List<StarWarsCharactersResponse> starWarsCharacters = new ArrayList<>();

    private StarWarsCharactersRecyclerAdapter adapter;
    private RecyclerClickListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setTitle(R.string.star_wars_title);
        presenter.loadStarWarsCharacters();

        initListeners();
        initAdapter();

    }

    private void initAdapter(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        adapter = new StarWarsCharactersRecyclerAdapter(starWarsCharacters,listener);
        starWarsCharactersRecyclerView.setLayoutManager(linearLayoutManager);
        starWarsCharactersRecyclerView.setAdapter(adapter);
    }

    private void initListeners(){
        btnRetry.setOnClickListener(view -> {
            presenter.loadStarWarsCharacters();

        });

        listener = item -> {
            Intent intent = new Intent(this, StarWarsCharactersDetailActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.StarWarsCharacterBundle.NAME,item.name);
            bundle.putString(Constants.StarWarsCharacterBundle.HEIGHT,item.height);
            bundle.putString(Constants.StarWarsCharacterBundle.MASS,item.mass);
            bundle.putString(Constants.StarWarsCharacterBundle.CREATED_DATE,item.dateCreated);
            intent.putExtra(Constants.StarWarsCharacterBundle.BUNDLE_KEY, bundle);
            startActivity(intent);
        };
    }

    @Override
    public void displayError(Throwable error) {
        Timber.e(error.getMessage());
        Toast.makeText(this, R.string.error_making_request, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setLoadingIndicator(boolean active) {
        loadingIndicator.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setData(StarWarsCharactersObject response) {
        this.starWarsCharacters = response.results;
        if(adapter!=null){
            adapter.setStarWarsCharacters(response.results);
        }else{
            initAdapter();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.stop();
    }
}
