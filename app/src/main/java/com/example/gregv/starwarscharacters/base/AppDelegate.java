package com.example.gregv.starwarscharacters.base;

import android.app.Activity;
import android.app.Application;

import com.example.gregv.starwarscharacters.BuildConfig;
import com.example.gregv.starwarscharacters.di.AppComponent;
import com.example.gregv.starwarscharacters.di.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import timber.log.Timber;

public class AppDelegate extends Application implements HasActivityInjector{

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    protected AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        component = initComponent();
        component.inject(this);

    }

    protected AppComponent initComponent(){
        return DaggerAppComponent
                .builder()
                .appDelegate(this)
                .build();

    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }
}
