package com.example.gregv.starwarscharacters.views.starwarsDetail;

import com.example.gregv.starwarscharacters.base.BasePresenter;
import com.example.gregv.starwarscharacters.rx.SchedulersFacade;

public class StarWarsCharactersDetailPresenter extends BasePresenter<
        StarWarsCharactersDetailContract.StarWarsCharactersDetailView> implements
        StarWarsCharactersDetailContract.StarWarsCharactersDetailPresenter {

    //This presenter doesnt do anything as of yet. It would be hooked up to a modified request
    //and data base in order to allow the user to get the data or refresh it. I put it in as an
    //example of how a project built like this would grow
    private final SchedulersFacade schedulersFacade;

    public StarWarsCharactersDetailPresenter(
            StarWarsCharactersDetailContract.StarWarsCharactersDetailView view,
            SchedulersFacade schedulersFacade) {
        super(view);
        this.schedulersFacade = schedulersFacade;
    }

}
