package com.example.gregv.starwarscharacters.views.starwars;

import com.example.gregv.starwarscharacters.model.StarWarsCharactersResponse;

public interface RecyclerClickListener {

    void onClick(StarWarsCharactersResponse item);

}
