package com.example.gregv.starwarscharacters.base;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenter<V> {

    public class RequestState  {
        public static final int IDLE = 0;
        public static final int LOADING = 1;
        public static final int COMPLETE = 2;
        public static final int ERROR = 3;

    }

    protected final V view;

    private CompositeDisposable disposables = new CompositeDisposable();

    protected BasePresenter(V view){
        this.view = view;
    }

    public void stop(){
        disposables.clear();
    }

    public void start() {
    }

    protected void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }

}
