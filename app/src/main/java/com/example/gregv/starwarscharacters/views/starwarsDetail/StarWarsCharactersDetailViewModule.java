package com.example.gregv.starwarscharacters.views.starwarsDetail;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class StarWarsCharactersDetailViewModule {

    @Binds
    abstract StarWarsCharactersDetailContract.StarWarsCharactersDetailView provideView(StarWarsCharactersDetailActivity activity);

}
