package com.example.gregv.starwarscharacters.networking;

import com.example.gregv.starwarscharacters.model.StarWarsCharactersResponse;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface StarWarsCharactersService {

    @GET("people/")
    Single<StarWarsCharactersObject> getStarWarsCharacters();

}
