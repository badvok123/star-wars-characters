package com.example.gregv.starwarscharacters.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Helpers {

    public static String formatDate(String dateString){
        dateString = dateString.replace("T", " ").replace("Z", "");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
        SimpleDateFormat newSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        try {
            Date date = sdf.parse(dateString);
            String formattedDate = newSdf.format(date);
            System.out.println(date);
            return formattedDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateString;
    }

}
