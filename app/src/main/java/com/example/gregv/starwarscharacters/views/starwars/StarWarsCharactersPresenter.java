package com.example.gregv.starwarscharacters.views.starwars;

import android.annotation.SuppressLint;
import android.util.Log;

import com.example.gregv.starwarscharacters.base.BasePresenter;
import com.example.gregv.starwarscharacters.model.StarWarsCharactersResponse;
import com.example.gregv.starwarscharacters.networking.StarWarsCharactersObject;
import com.example.gregv.starwarscharacters.rx.SchedulersFacade;
import com.jakewharton.rxrelay2.BehaviorRelay;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import timber.log.Timber;

import static com.example.gregv.starwarscharacters.base.BasePresenter.RequestState.COMPLETE;
import static com.example.gregv.starwarscharacters.base.BasePresenter.RequestState.ERROR;
import static com.example.gregv.starwarscharacters.base.BasePresenter.RequestState.LOADING;

public class StarWarsCharactersPresenter extends BasePresenter<
        StarWarsCharactersContract.StarWarsCharactersView> implements
        StarWarsCharactersContract.StarWarsCharactersPresenter{


    private final SchedulersFacade schedulersFacade;
    private final StarWarsCharactersUseCase starWarsCharactersUseCase;

    private final BehaviorRelay<Integer> requestStateObserver
            = BehaviorRelay.createDefault(RequestState.IDLE);

    StarWarsCharactersPresenter(StarWarsCharactersContract.StarWarsCharactersView view,
                                StarWarsCharactersUseCase starWarsCharactersUseCase,
                                SchedulersFacade schedulersFacade){
        super(view);
        this.starWarsCharactersUseCase = starWarsCharactersUseCase;
        this.schedulersFacade = schedulersFacade;
        observeRequestState();
    }

    @Override
    public void loadStarWarsCharacters() {
        loadStarWarsCharacters(starWarsCharactersUseCase.execute());
    }

    private void loadStarWarsCharacters(Single<StarWarsCharactersObject> response){
        addDisposable(response
                .subscribeOn(schedulersFacade.io())
                .observeOn(schedulersFacade.ui())
                .doOnSubscribe(s -> publishRequestState(LOADING))
                .doOnSuccess(s -> publishRequestState(COMPLETE))
                .doOnError(t -> publishRequestState(ERROR))
                .subscribe(view::setData, view::displayError)
        );
    }

    private void publishRequestState(int requestState){
        addDisposable(Observable.just(requestState)
            .observeOn(schedulersFacade.ui())
                .subscribe(requestStateObserver)
        );
    }

    @SuppressLint("CheckResult")
    private void observeRequestState() {
        requestStateObserver.subscribe(requestState -> {
            switch (requestState) {
                case RequestState.IDLE:
                    break;
                case LOADING:
                    view.setLoadingIndicator(true);
                    break;
                case COMPLETE:
                    view.setLoadingIndicator(false);
                    break;
                case ERROR:
                    view.setLoadingIndicator(false);
                    break;
            }
        }, Timber::e);
    }
}
