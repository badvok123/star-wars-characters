package com.example.gregv.starwarscharacters.networking;

import com.example.gregv.starwarscharacters.model.StarWarsCharactersResponse;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import retrofit2.Retrofit;

public class StarWarsCharactersRepository {

    private Retrofit retrofit;

    @Inject
    StarWarsCharactersRepository(Retrofit retrofit){
        this.retrofit = retrofit;
    }

    public Single<StarWarsCharactersObject> makeRequest(){
        StarWarsCharactersService service = retrofit.create(StarWarsCharactersService.class);
        return service.getStarWarsCharacters();
    }

}
