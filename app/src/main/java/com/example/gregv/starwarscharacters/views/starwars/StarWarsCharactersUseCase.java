package com.example.gregv.starwarscharacters.views.starwars;

import com.example.gregv.starwarscharacters.model.StarWarsCharactersResponse;
import com.example.gregv.starwarscharacters.networking.StarWarsCharactersObject;
import com.example.gregv.starwarscharacters.networking.StarWarsCharactersRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class StarWarsCharactersUseCase {

    private final StarWarsCharactersRepository repository;

    @Inject
    StarWarsCharactersUseCase(StarWarsCharactersRepository repository){
        this.repository = repository;
    }

    public Single<StarWarsCharactersObject> execute(){
        return repository.makeRequest();
    }



}
