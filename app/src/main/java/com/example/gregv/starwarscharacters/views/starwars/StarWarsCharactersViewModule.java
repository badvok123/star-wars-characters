package com.example.gregv.starwarscharacters.views.starwars;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class StarWarsCharactersViewModule {

    @Binds
    abstract StarWarsCharactersContract.StarWarsCharactersView provideView(StarWarsCharactersActivity activity);

}
