package com.example.gregv.starwarscharacters.rx;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SchedulersFacade {

    //class containing rxjavas schedulers, something that would be needed by multiple classes should
    //the project expand 
    @Inject
    public SchedulersFacade() {
    }

    public Scheduler io() {
        return Schedulers.io();
    }

    public Scheduler computation() {
        return Schedulers.computation();
    }

    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }

}
