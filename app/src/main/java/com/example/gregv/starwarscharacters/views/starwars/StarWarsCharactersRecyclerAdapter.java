package com.example.gregv.starwarscharacters.views.starwars;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.gregv.starwarscharacters.R;
import com.example.gregv.starwarscharacters.model.StarWarsCharactersResponse;
import com.example.gregv.starwarscharacters.util.Helpers;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

//import com.example.gregv.starwarscharacters.R;

public class StarWarsCharactersRecyclerAdapter extends
        RecyclerView.Adapter<StarWarsCharactersRecyclerAdapter.StarWarsCharacterViewHolder> {

    private List<StarWarsCharactersResponse> starWarsCharacters;
    private RecyclerClickListener listener;

    public StarWarsCharactersRecyclerAdapter(List<StarWarsCharactersResponse> starWarsCharacters, RecyclerClickListener listener) {
        this.starWarsCharacters = starWarsCharacters;
        this.listener = listener;
    }

    @NonNull
    @Override
    public StarWarsCharactersRecyclerAdapter.StarWarsCharacterViewHolder onCreateViewHolder(
            @NonNull ViewGroup parent,
            int viewType) {

        return new StarWarsCharacterViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.row_star_wars_character, parent, false
                )
        );
    }

    @Override
    public void onBindViewHolder(
            @NonNull StarWarsCharactersRecyclerAdapter.StarWarsCharacterViewHolder holder,
            int position) {
        holder.bind(starWarsCharacters.get(position));

    }

    public void setStarWarsCharacters(List<StarWarsCharactersResponse> starWarsCharacters) {
        this.starWarsCharacters = starWarsCharacters;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return starWarsCharacters.size();
    }

    public class StarWarsCharacterViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_name) TextView txtName;
        @BindView(R.id.txt_height) TextView txtHeight;
        @BindView(R.id.txt_mass) TextView txtMass;
        @BindView(R.id.txt_created_date) TextView txtDateCreated;

        private View itemView;

        public StarWarsCharacterViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            ButterKnife.bind(this, itemView);


        }

        public void bind(StarWarsCharactersResponse item) {
            if (item != null) {
                txtName.setText(item.name);
                txtHeight.setText(item.height);
                txtMass.setText(item.mass);
                String dateCreated = Helpers.formatDate(item.dateCreated);
                txtDateCreated.setText(dateCreated);
            }
            itemView.setOnClickListener(view -> listener.onClick(item));
        }
    }
}
