package com.example.gregv.starwarscharacters.views.starwars;

import com.example.gregv.starwarscharacters.model.StarWarsCharactersResponse;
import com.example.gregv.starwarscharacters.networking.StarWarsCharactersObject;

import java.util.List;

public interface StarWarsCharactersContract {

    interface StarWarsCharactersView {

        void displayError(Throwable error);

        void setLoadingIndicator(boolean active);

        void setData(StarWarsCharactersObject response);
    }

    interface StarWarsCharactersPresenter {
        void loadStarWarsCharacters();
    }

}
