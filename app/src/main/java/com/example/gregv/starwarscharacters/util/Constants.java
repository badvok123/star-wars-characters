package com.example.gregv.starwarscharacters.util;

public class Constants {

    public class StarWarsCharacterBundle{
        public static final String BUNDLE_KEY = "starwars_character_bundle_key";
        public static final String NAME = "starwars_character_name";
        public static final String HEIGHT = "starwars_character_height";
        public static final String MASS = "starwars_character_mass";
        public static final String CREATED_DATE = "starwars_character_created_date";
    }

}
